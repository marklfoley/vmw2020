#!/bin/sh

set -eu

. "$(dirname "${0}")/functions.sh"

buildImage() {
  local dir="${1}"
  local tag="$(imageTag)"
  local image="$(echo "${dir}" | sed 's,_,-,g')"

  docker build -t "${DOCKER_REPOSITORY_PREFIX}${image}:${tag}" "${dir}"
  docker login -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD
  docker push "${DOCKER_REPOSITORY_PREFIX}${image}:${tag}"
}


if [ "$#" = "0" ] ; then
  dirs="cart catalogsvc front_end order payment user"
else
  dirs="$@"
fi

for dir in "${dirs}" ; do
  buildImage "${dir}"
done
